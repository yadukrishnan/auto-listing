# AUTO ADVERTISEMENT #

This sample project support the CRUD Methods for Car Advertisements and Fuel Types

To run the program, follow the below steps.

- Update application.conf under __src/main/resources__ with your postgres database details.
- Run the Rest Server **HttpServer**
- Application will start on __http://localhost:8282/auto24__
- Tests are configured to run with h2 database

The BaseRest provides 5 generic endpoints(GetAll, GetById, Insert, UpdateById, DeleteById) for all entities.

Similarly, RelationalRepository provides the corresponding 5 repository methods for the generic operations.

Supported Methods:

| Method  | URL | Description |
| ------------- | ------------- | --------- |
| GET | http://localhost:8282/auto24/carAdverts |To list all carAdverts |
| GET | http://localhost:8282/auto24/carAdverts?sortColumn=title |To list all carAdverts sorted by title|
| GET | http://localhost:8282/auto24/carAdverts/{id} | To get carAdverts by id |
| POST | http://localhost:8282/auto24/carAdverts | To a create new  carAdverts |
| PUT | http://localhost:8282/auto24/carAdverts/{id} | To modify an existing carAdverts |
| DELETE | http://localhost:8282/auto24/carAdverts/{id} | To delete an existing carAdverts |


