package com.yadu.auto24.exceptions

/**
 * Created by yadu on 25/01/20
 */


case class InvalidInputException(message: String) extends Exception {
  override def getMessage: String = message
}