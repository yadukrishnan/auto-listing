package com.yadu.auto24.entities

import java.time.LocalDate

import com.yadu.auto24.exceptions.InvalidInputException
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._
import slick.lifted.{Rep, Tag}

import scala.concurrent.Future

/**
 * Created by yadu on 25/01/20
 */

/** ** core entities **/
trait BaseEntity {
  def id: Long
}

abstract class BaseTable[E <: BaseEntity](tag: Tag, schemaName: Option[String], tableName: String)(implicit profile: JdbcProfile) extends Table[E](tag, schemaName, tableName) {
  def id: Rep[Long]
}

case class CountContainer(count: Int)

case class IdContainer(id: Long)

/** ** module specific entities **/
case class CarAdvert(id: Long, title: String, fuelTypeId: Long, price: Long, isNew: Boolean, mileage: Option[Long], firstRegistrationDate: Option[LocalDate]) extends BaseEntity

case class FuelType(id: Long, name: String) extends BaseEntity


