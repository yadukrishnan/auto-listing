package com.yadu.auto24.entities

import java.time.LocalDate

import slick.jdbc.PostgresProfile

/**
 * Created by yadu on 25/01/20
 */
object Tables extends {
  val profile = PostgresProfile
} with Tables

trait Tables {
  val profile: PostgresProfile

  import profile.api._

  class CarAdvertTable(_tableTag: Tag) extends BaseTable[CarAdvert](_tableTag, None, "car_advert") {
    def * = (id, title, fuelTypeId, price, isNew, mileage, firstRegistrationDate) <> (CarAdvert.tupled, CarAdvert.unapply)

    def ? = (Rep.Some(id), Rep.Some(title), Rep.Some(fuelTypeId), Rep.Some(price), Rep.Some(isNew), mileage, firstRegistrationDate).shaped.<>({ r =>
      import r._;
      _1.map(_ => CarAdvert.tupled((_1.get, _2.get, _3.get, _4.get,
        _5.get, _6, _7)))
    }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    val id                   : Rep[Long]              = column[Long]("id", O.AutoInc, O.PrimaryKey)
    val title                : Rep[String]            = column[String]("title")
    val fuelTypeId           : Rep[Long]              = column[Long]("fuel_type_id")
    val price                : Rep[Long]              = column[Long]("price")
    val isNew                : Rep[Boolean]           = column[Boolean]("new")
    val mileage              : Rep[Option[Long]]      = column[Option[Long]]("mileage")
    val firstRegistrationDate: Rep[Option[LocalDate]] = column[Option[LocalDate]]("first_registration")
  }
  lazy val carAdvertTable = new TableQuery(tag => new CarAdvertTable(tag))


  class FuelTypeTable(_tableTag: Tag) extends BaseTable[FuelType](_tableTag, None, "fuel_type") {
    def * = (id, name) <> (FuelType.tupled, FuelType.unapply)

    def ? = (Rep.Some(id), Rep.Some(name)).shaped.<>({ r =>
      import r._;
      _1.map(_ => FuelType.tupled((_1.get, _2.get)))
    }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    val id  : Rep[Long]   = column[Long]("id", O.AutoInc, O.PrimaryKey)
    val name: Rep[String] = column[String]("name")

  }

  lazy val fuelTypeTable = new TableQuery(tag => new FuelTypeTable(tag))


}

