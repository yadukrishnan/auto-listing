package com.yadu.auto24

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.model.{HttpEntity, HttpResponse, MediaTypes, StatusCodes}
import com.yadu.auto24.exceptions.InvalidInputException
import org.json4s.JsonAST.{JNull, JString}
import org.json4s.jackson.Serialization
import org.json4s.{CustomSerializer, DefaultFormats, Formats}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by yadu on 25/01/20
 */


trait JsonProcessor extends DateSerializer {

  implicit val formats: Formats = new DefaultFormats {
    override def dateFormatter = new SimpleDateFormat("yyyy-MM-dd")
  } ++ List(LocalDateSerializer)

  def toJson(obj: AnyRef): String = {
    Serialization.write(obj)
  }

  def fromJson[E](json: String)(implicit m: Manifest[E]): E = {
    val parsedResult = Serialization.read[E](json)
    parsedResult
  }

  def futureToJson(obj: Future[AnyRef]): Future[HttpResponse] = {
    obj.map{ x =>
      HttpResponse(status = StatusCodes.OK, entity = HttpEntity(MediaTypes.`application/json`, Serialization.write(x)))
    }.recover{
      case ex => ex.printStackTrace(); handleExceptions(ex)
    }
  }

  def handleExceptions(throwable: Throwable) = {
    throwable match {
      case ex: InvalidInputException => HttpResponse(status = StatusCodes.PreconditionFailed, entity = HttpEntity(ex.getMessage))
      case ex                        => HttpResponse(status = StatusCodes.InternalServerError)
    }
  }

  def handleJsonParseException(ex:Throwable) = {
    val cause = ex.getCause
    val msg = if(cause != null) ex.getMessage + " : cause : "+ cause else ex.getMessage
    HttpResponse(status = StatusCodes.BadRequest, entity = HttpEntity(msg))
  }
}

trait DateSerializer {

  case object LocalDateSerializer extends CustomSerializer[java.time.LocalDate](format => ( {
    case JString(date) => {
      val javaDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
      javaDate
    }
    case JNull         => null
  }, {
    case date: java.time.LocalDate => JString(date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
  }))

}