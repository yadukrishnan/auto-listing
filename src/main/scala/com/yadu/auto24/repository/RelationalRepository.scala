package com.yadu.auto24.repository

import com.yadu.auto24.entities.{BaseEntity, BaseTable, CountContainer, IdContainer}
import com.yadu.auto24.exceptions.InvalidInputException
import slick.jdbc.JdbcProfile
import slick.lifted.TableQuery

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by yadu on 25/01/20
 */
trait DependenciesComponent {
  def profile: JdbcProfile

  def databaseManager: DatabaseManagerComponent
}

abstract class RelationalRepository[T <: BaseTable[E], E <: BaseEntity](query: TableQuery[T])(implicit deps: DependenciesComponent) {

  val db = deps.databaseManager.db
  implicit val jdbcProfile = deps.profile.api

  import jdbcProfile._

  def getAllRecords(sortField: Option[String] = None): Future[Seq[E]] = {
    db.run(query.sortBy(_.id).result)
  }

  def getRecordById(id: Long): Future[Option[E]] = {
    db.run(query.filter(_.id === id).result.headOption)
  }

  def insert(record: E) = {
    //    val insertQuery = query returning query.map(obj => obj) += record
    val insertQuery = query returning query.map(obj => obj.id) += record
    db.run(insertQuery).map(s => IdContainer(s))
  }

  def update(id: Long, record: E): Future[CountContainer] = {
    if (record.id != id) {
      Future.failed(InvalidInputException(s"id field in the object[${record.id}] is not matching with the parameter id[${id}]"))
    } else {
      val modifiedQuery = query.filter(_.id === id).update(record)
      db.run(modifiedQuery).map(c => CountContainer(c))
    }
  }

  def delete(id: Long): Future[CountContainer] = {
    db.run(query.filter(_.id === id).delete).map(c => CountContainer(c))
  }

}
