package com.yadu.auto24.repository

import slick.jdbc.JdbcBackend.Database

/**
 * Created by yadu on 25/01/20
 */


trait DatabaseManagerComponent {
  def db: Database
}

object PGDatabaseManager extends DatabaseManagerComponent {
  val db = Database.forConfig("database")
}