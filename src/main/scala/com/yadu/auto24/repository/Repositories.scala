package com.yadu.auto24.repository

import java.time.LocalDate

import com.yadu.auto24.entities.Tables.{CarAdvertTable, FuelTypeTable}
import com.yadu.auto24.entities.{CarAdvert, CountContainer, FuelType, IdContainer}
import com.yadu.auto24.exceptions.InvalidInputException
import slick.lifted.TableQuery

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by yadu on 25/01/20
 */


class FuelTypeRepository(implicit deps: DependenciesComponent) extends RelationalRepository[FuelTypeTable, FuelType](TableQuery[FuelTypeTable]) {

}


class CarAdvertRepository(fuelTypeRepo: FuelTypeRepository)(implicit deps: DependenciesComponent) extends RelationalRepository[CarAdvertTable, CarAdvert](TableQuery[CarAdvertTable]) {
  override def insert(record: CarAdvert): Future[IdContainer] = {
    if (!record.isNew && (record.mileage.isEmpty || record.firstRegistrationDate.isEmpty)) {
      Future.failed(InvalidInputException("FirstRegistrationDate and mileage is mandatory for used cars"))
    } else {
      for {
        isValid <- isValidFuelType(record)
        result <- if (!isValid) Future.failed(InvalidInputException(s"FuelTypeId : ${record.fuelTypeId} doesn't exist in the system"))
        else super.insert(record)
      } yield result
    }
  }

  def isValidFuelType(carAdvert: CarAdvert) = {
    val result = fuelTypeRepo.getRecordById(carAdvert.fuelTypeId).map(_.isDefined)
    result.recoverWith{
      case _ => Future.successful(false)
    }
  }

  override def update(id: Long, record: CarAdvert): Future[CountContainer] = {
    if (!record.isNew && (record.mileage.isEmpty || record.firstRegistrationDate.isEmpty)) {
      Future.failed(InvalidInputException("FirstRegistrationDate and mileage is mandatory for used cars"))
    } else {
      for {
        isValid <- isValidFuelType(record)
        result <- if (!isValid) Future.failed(InvalidInputException(s"FuelTypeId : ${record.fuelTypeId} doesn't exist in the system"))
        else super.update(id, record)
      } yield result
    }
  }

  override def getAllRecords(sortField: Option[String] = None): Future[Seq[CarAdvert]] = {
    val resultsFuture = super.getAllRecords()
    if (sortField.isEmpty) resultsFuture
    else {
      resultsFuture.map{ results =>
        orderByColumn(sortField.get, results)
      }
    }
  }

  //  Ordering.by[CarAdvert](_.title)
  //
  //  val sortById = (p :CarAdvert) => p.id

  //  def sortOrdering(columnName: String) = {
  //    columnName match {
  //      case "id"                    => (p :CarAdvert) => p.id
  //      case "title"                 => (p :CarAdvert) => p.title
  //      case "fuelTypeId"            => (p :CarAdvert) => p.fuelTypeId
  //      case "price"                 => (p :CarAdvert) => p.price
  //      case "isNew"                 => (p :CarAdvert) => p.isNew
  //      case "mileage"               => (p :CarAdvert) => p.mileage
  //      case "firstRegistrationDate" => (p :CarAdvert) => p.firstRegistrationDate
  //    }
  //  }

  def orderByColumn(columnName: String, list: Seq[CarAdvert]) = {

    implicit val localDateOrdering: Ordering[LocalDate] = _ compareTo _

    columnName match {
      case "id"                    => list.sortBy(_.id)
      case "title"                 => list.sortBy(_.title)
      case "fuelTypeId"            => list.sortBy(_.fuelTypeId)
      case "price"                 => list.sortBy(_.price)
      case "isNew"                 => list.sortBy(_.isNew)
      case "mileage"               => list.sortBy(_.mileage)
      case "firstRegistrationDate" => list.sortBy(_.firstRegistrationDate)
      case _                       => throw InvalidInputException(s"Sorting field ${columnName} doesn't exist in the entity")
    }
  }
}