package com.yadu.auto24.repository

import slick.jdbc.{JdbcProfile, PostgresProfile}

/**
 * Created by yadu on 25/01/20
 */


object PGDependencies extends DependenciesComponent {
  override lazy val profile: JdbcProfile = PostgresProfile

  override lazy val databaseManager: DatabaseManagerComponent = PGDatabaseManager
}
