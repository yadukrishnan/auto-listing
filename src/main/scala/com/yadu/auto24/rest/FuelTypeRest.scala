package com.yadu.auto24.rest

import com.yadu.auto24.entities.FuelType
import com.yadu.auto24.entities.Tables.FuelTypeTable
import com.yadu.auto24.repository.RelationalRepository

/**
 * Created by yadu on 25/01/20
 */


class FuelTypeRest(repository: RelationalRepository[FuelTypeTable, FuelType]) extends BaseRest[FuelTypeTable, FuelType]("fuelTypes", repository) {

}
