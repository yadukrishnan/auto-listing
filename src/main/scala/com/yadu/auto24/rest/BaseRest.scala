package com.yadu.auto24.rest

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives
import com.yadu.auto24.JsonProcessor
import com.yadu.auto24.entities.{BaseEntity, BaseTable}
import com.yadu.auto24.exceptions.InvalidInputException
import com.yadu.auto24.repository.RelationalRepository

import scala.util.Try

/**
 * Created by yadu on 25/01/20
 */

abstract class BaseRest[T <: BaseTable[E], E <: BaseEntity](val pathName: String, repository: RelationalRepository[T, E])(implicit m: Manifest[E])
  extends Directives with JsonProcessor {

  def route = path(pathName){
    get{
      parameter("sortColumn" ? "id"){ column =>
        complete(futureToJson(repository.getAllRecords(Some(column))))
      }
    } ~ post{
      entity(as[String]){ json =>
        val extractedEntity = Try(fromJson[E](json))
        if(extractedEntity.isSuccess)
        complete(futureToJson(repository.insert(extractedEntity.get)))
        else complete(handleJsonParseException(extractedEntity.failed.get))
      }
    }
  } ~ path(pathName / LongNumber){ id =>
    get{
      complete(futureToJson(repository.getRecordById(id)))
    } ~ put{
      entity(as[String]){ json =>
        val extractedEntity = Try(fromJson[E](json))
        if(extractedEntity.isSuccess)
        complete(futureToJson(repository.update(id, extractedEntity.get)))
        else complete(handleJsonParseException(extractedEntity.failed.get))
      }
    } ~ delete{
      complete(futureToJson(repository.delete(id)))
    }
  }


}