package com.yadu.auto24.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import akka.stream.ActorMaterializer
import com.yadu.auto24.repository.{CarAdvertRepository, FuelTypeRepository, PGDependencies}
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by yadu on 25/01/20
 */


object HttpServer extends RouteConcatenation with CORSSupport with App {

  override lazy val contextRoot = "auto24"
  val server = "localhost"
  val port   = 8282

  implicit val deps = PGDependencies

  implicit val system = ActorSystem("Auto24System")

  implicit val mat = ActorMaterializer()

  val fuelTypeRepository = new FuelTypeRepository()

  def allRoutes: Route = new CarAdvertRest(new CarAdvertRepository(fuelTypeRepository)).route ~
    new FuelTypeRest(fuelTypeRepository).route

  val routesWithCORS = cors(allRoutes)

  Http().bindAndHandle(routesWithCORS, server, port).map{ _ =>
    println(s"Successfully bound to http://${server}:${port}/${contextRoot}")
  }.recover{
    case ex =>
      ex.printStackTrace
  }

}
