package com.yadu.auto24.rest

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Route
import com.yadu.auto24.entities.CarAdvert
import com.yadu.auto24.entities.Tables.CarAdvertTable
import com.yadu.auto24.repository.RelationalRepository

/**
 * Created by yadu on 25/01/20
 */


class CarAdvertRest(repository: RelationalRepository[CarAdvertTable, CarAdvert]) extends BaseRest[CarAdvertTable, CarAdvert]("carAdverts", repository) {
  override def route: Route = {
    super.route ~ path("carAdverts/corsMethod"){
      complete(HttpEntity(ContentTypes.`text/html(UTF-8)`,
        "<html><body>Hello world!</body></html>"))
    }
  }
}
