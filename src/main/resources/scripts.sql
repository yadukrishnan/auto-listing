--CREATE SCHEMA ads;

CREATE TABLE fuel_type (
    id BIGSERIAL PRIMARY KEY,
    name CHARACTER VARYING(50) NOT NULL
);

CREATE TABLE car_advert (
    id bigserial PRIMARY KEY,
    title CHARACTER VARYING(50) NOT NULL,
    fuel_type_id BIGINT,
    price BIGINT NOT NULL,
    new BOOLEAN NOT NULL,
    mileage BIGINT,
    first_registration DATE);
