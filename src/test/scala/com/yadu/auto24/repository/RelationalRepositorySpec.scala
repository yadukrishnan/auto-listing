package com.yadu.auto24.repository

import java.io.File

import com.yadu.auto24.entities.Tables.FuelTypeTable
import com.yadu.auto24.entities.{FuelType, Tables}
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import slick.dbio.DBIOAction

import scala.concurrent.duration._
import scala.io.Source
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by yadu on 25/01/20
 */


class RelationalRepositorySpec extends WordSpec with Matchers with ScalaFutures with BeforeAndAfterAll {

  implicit val deps = H2Dependencies
  val timeout = Timeout(10.seconds)

  override def beforeAll() = {
    import deps.profile.api._
    val schemas = List(Tables.fuelTypeTable, Tables.carAdvertTable)
    val createActions = DBIOAction.seq(schemas.map(s => s.schema.create):_*)
    val dropActions = DBIOAction.seq(schemas.map(s => s.schema.dropIfExists):_*)

    deps.databaseManager.db.run(dropActions).futureValue(timeout)

    deps.databaseManager.db.run(createActions).futureValue(timeout)
  }

  "Relational repository database spec" should {

    "insert, update and delete the records correctly" in {
      val fuelTypeRepo = new FuelTypeRepository
      val fuelType1    = FuelType(0L, "Diesel")
      val fuelType2    = FuelType(0L, "Petrol")

      // insert 2 records and verify if the method has returned the generated primary key
      val insertedId1 = fuelTypeRepo.insert(fuelType1).futureValue(timeout).id
      insertedId1 should be > 0L

      val insertedId2 = fuelTypeRepo.insert(fuelType2).futureValue(timeout).id
      insertedId2 should be > 0L

      //get all test
      val allRecords = fuelTypeRepo.getAllRecords().futureValue(timeout)
      allRecords should have size 2
      allRecords shouldBe List(fuelType1.copy(id = insertedId1), fuelType2.copy(insertedId2))

      //modify one record by id
      val updatedRecordSize = fuelTypeRepo.update(insertedId2, fuelType2.copy(id = insertedId2, name = "Gasoline"))
      updatedRecordSize.futureValue(timeout).count shouldBe 1L

      //get the record by id and verify updated record
      val gasoline = fuelTypeRepo.getRecordById(insertedId2).futureValue(timeout)
      gasoline should not be empty
      gasoline.get.name shouldBe "Gasoline"

      //delete the record
      val deleteStatus = fuelTypeRepo.delete(insertedId1)
      deleteStatus.futureValue(timeout).count shouldBe 1L

      //verify if record is deleted by getting all records
      val allRecordsAgain = fuelTypeRepo.getAllRecords().futureValue(timeout)
      allRecordsAgain should have size 1
      allRecordsAgain shouldBe List(gasoline.get)

    }

  }

}
