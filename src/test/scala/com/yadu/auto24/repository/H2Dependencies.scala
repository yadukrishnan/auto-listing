package com.yadu.auto24.repository

import slick.jdbc.JdbcBackend.Database
import slick.jdbc.{H2Profile, JdbcProfile, PostgresProfile}

/**
 * Created by yadu on 25/01/20
 */

object H2DatabaseManager extends DatabaseManagerComponent {
  val db = Database.forConfig("h2")
}

object H2Dependencies extends DependenciesComponent {
  override lazy val profile: JdbcProfile = H2Profile

  override lazy val databaseManager: DatabaseManagerComponent = H2DatabaseManager
}