package com.yadu.auto24.rest

import java.time.LocalDate

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.yadu.auto24.entities.{BaseEntity, BaseTable, CountContainer, IdContainer}
import com.yadu.auto24.repository.{DatabaseManagerComponent, DependenciesComponent, FuelTypeRepository, PGDependencies, RelationalRepository}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}
import slick.jdbc.JdbcProfile
import slick.lifted.Tag
import org.mockito.Matchers._
import org.mockito.Mockito._
import akka.http.scaladsl.server._
import Directives._
import com.yadu.auto24.JsonProcessor
import com.yadu.auto24.exceptions.InvalidInputException

import scala.concurrent.Future

/**
 * Created by yadu on 26/01/20
 */


class BaseRestSpec extends WordSpec with Matchers with ScalaFutures with MockitoSugar with ScalatestRouteTest with JsonProcessor {

  "Base Rest Spec" should {
    val mockRepo = mock[RelationalRepository[MockTables.MockTable, MockEntity]]
    val basePath = "mockEntities"
    val baseRest = new MockRest(basePath, mockRepo) {}

    "return all records when get all records are invoked" in {
      val allMocks = Seq(
        MockEntity(1L, "Mock Entity 1", LocalDate.parse("2020-01-01")),
        MockEntity(2L, "Mock Entity 2", LocalDate.parse("2020-01-02"))
      )
      when(mockRepo.getAllRecords(any[Option[String]])).thenReturn(Future.successful(allMocks))

      Get("/mockEntities") ~> baseRest.route ~> check{
        val responseStr      = responseAs[String]
        val repsonseEntities = fromJson[List[MockEntity]](responseStr)
        response.status.intValue() shouldBe 200
        repsonseEntities shouldBe allMocks
      }
    }

    "return all records when get all records are invoked with sort field as name" in {
      val allMocks = Seq(
        MockEntity(1L, "Mock Entity 1", LocalDate.parse("2020-01-01")),
        MockEntity(2L, "Mock Entity 2", LocalDate.parse("2020-01-02"))
      )
      when(mockRepo.getAllRecords(Some("name"))).thenReturn(Future.successful(allMocks))

      Get("/mockEntities?sortColumn=name") ~> baseRest.route ~> check{
        val responseStr      = responseAs[String]
        val repsonseEntities = fromJson[List[MockEntity]](responseStr)
        response.status.intValue() shouldBe 200
        repsonseEntities shouldBe allMocks
      }
    }

    "return selected record by id" in {
      val entity = MockEntity(1L, "Mock Entity 1", LocalDate.parse("2020-01-01"))
      when(mockRepo.getRecordById(1L)).thenReturn(Future.successful(Some(entity)))

      Get("/mockEntities/1") ~> baseRest.route ~> check{
        val responseStr = responseAs[String]
        val result      = fromJson[Option[MockEntity]](responseStr)
        response.status.intValue() shouldBe 200
        result.isDefined shouldBe true
        result.get shouldBe entity
      }

    }

    "insert a new record successfully" in {
      val entity = MockEntity(1L, "Mock Entity 1", LocalDate.parse("2020-01-01"))
      when(mockRepo.insert(entity)).thenReturn(Future.successful(IdContainer(1)))

      Post("/mockEntities", toJson(entity)) ~> baseRest.route ~> check{
        val responseStr = responseAs[String]
        val result      = fromJson[IdContainer](responseStr)
        response.status.intValue() shouldBe 200
        result shouldBe IdContainer(1)
      }

    }

    "throw bad request if the input is not a valid json" in {
      val entity = MockEntity(1L, "Mock Entity 1", LocalDate.parse("2020-01-01"))
      //      when(mockRepo.insert(entity)).thenReturn(Future.successful(IdContainer(1)))

      Post("/mockEntities", "invalid") ~> baseRest.route ~> check{
        response.status.intValue() shouldBe 400
      }
    }

    "throw bad request if the input is not a valid json for update request" in {
      val entity = MockEntity(1L, "Mock Entity 1", LocalDate.parse("2020-01-01"))
      //      when(mockRepo.insert(entity)).thenReturn(Future.successful(IdContainer(1)))

      Put("/mockEntities/1", "invalid") ~> baseRest.route ~> check{
        response.status.intValue() shouldBe 400
      }
    }

    "throw Pre-Condition-Failed if the input id and id in json is not matchingfor update request" in {
      val entity = MockEntity(1L, "Mock Entity 1", LocalDate.parse("2020-01-01"))
      when(mockRepo.update(anyLong(), any[MockEntity])).thenReturn(Future.failed(InvalidInputException("Wrong id")))

      Put("/mockEntities/2", toJson(entity)) ~> baseRest.route ~> check{
        response.status.intValue() shouldBe 412
      }
    }

    "delete an existing record successfully" in {
      when(mockRepo.delete(1L)).thenReturn(Future.successful(CountContainer(1)))

      Delete("/mockEntities/1") ~> baseRest.route ~> check{
        val responseStr = responseAs[String]
        val result      = fromJson[CountContainer](responseStr)
        response.status.intValue() shouldBe 200
        result shouldBe CountContainer(1)
      }

    }

  }


}

/** ***** MOCK Setup ***********/

object MockDependencies extends DependenciesComponent with MockitoSugar {
  override implicit lazy val profile: JdbcProfile = mock[JdbcProfile]

  override lazy val databaseManager: DatabaseManagerComponent = mock[DatabaseManagerComponent]
}

object MockTables {
  implicit val mockDependencies = MockDependencies

  import MockDependencies._

  class MockTable(_tableTag: Tag) extends BaseTable[MockEntity](_tableTag, None, "mock_table") {
    override def id = ???

    override def * = ???
  }

}


case class MockEntity(id: Long, name: String, date: LocalDate) extends BaseEntity

class MockRest(basePath: String, mockRepo: RelationalRepository[MockTables.MockTable, MockEntity]) extends BaseRest[MockTables.MockTable, MockEntity](basePath, mockRepo)