package com.yadu.auto24.rest

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.yadu.auto24.JsonProcessor
import com.yadu.auto24.entities.{CarAdvert, CountContainer, FuelType, Tables}
import com.yadu.auto24.repository.{CarAdvertRepository, FuelTypeRepository, H2Dependencies}
import org.mockito.Mockito._
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import slick.dbio.DBIOAction

import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * Created by yadu on 23/01/20
 */


class CarAdvertRestSpec extends WordSpec with MockitoSugar with Matchers with ScalaFutures with ScalatestRouteTest with JsonProcessor with BeforeAndAfterAll {

  val timeout  = Timeout(3.seconds)
  val basePath = "/carAdverts"
  implicit val deps = H2Dependencies
  val mockFuelTypeRepo = mock[FuelTypeRepository]
  val carAdvertRest    = new CarAdvertRest(new CarAdvertRepository(mockFuelTypeRepo)) {}

  override def beforeAll() = {
    import deps.profile.api._
    val schemas = List(Tables.fuelTypeTable, Tables.carAdvertTable)
    val dropAction  = DBIOAction.seq(schemas.map(s => s.schema.dropIfExists): _*)
    val createAction  = DBIOAction.seq(schemas.map(s => s.schema.create): _*)
    deps.databaseManager.db.run(dropAction).futureValue(timeout)
    deps.databaseManager.db.run(createAction).futureValue(timeout)
  }

  val fuelTypeDiesel = FuelType(1L, "Diesel")
  when(mockFuelTypeRepo.getRecordById(1L)).thenReturn(Future.successful(Some(fuelTypeDiesel)))

  "CarAdvertRest Spec" should {

    "insert the car advert into the database successfully if all the validations are passed" in {
      val carAdvert = CarAdvert(1L, "Ad1", 1L, 100, true, None, None)
      Post(basePath, toJson(carAdvert)) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
      }
    }

    "return with 400-BadRequest if the mandatory fields are not provided" in {
      val carAdvert = """{"id":1}"""
      Post(basePath, carAdvert) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 400
        responseAs[String] should include("No usable value")
      }
    }

    "return with 400-badRequest if the format for the data is invalid (e.g: Date Format)" in {
      val carAdvert = """{"id":1, "title":"car", "isNew":true, "price":1, "firstRegistrationDate":"01-01-2020", "fuelTypeId":1}"""
      Post(basePath, carAdvert) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 400
        responseAs[String] should include("DateTimeParseException")
      }
    }


    "return 412-PreconditionFailed if the used car only fields are sent for new cars" in {
      val carAdvert = CarAdvert(1L, "Ad1", 1L, 100, false, None, None)
      Post(basePath, toJson(carAdvert)) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 412
        responseAs[String] should include("FirstRegistrationDate and mileage is mandatory for used cars")
      }
    }

    "return 412-PreConditionFailed if the fuel value is not valid" in {
      val carAdvertRest2 = new CarAdvertRest(new CarAdvertRepository(new FuelTypeRepository())) {}
      val carAdvert      = CarAdvert(1L, "Ad1", 29L, 100, true, None, None)
      Post(basePath, toJson(carAdvert)) ~> carAdvertRest2.route ~> check{
        response.status.intValue() shouldBe 412
      }
    }

    "insert one more car advert into the database successfully if all the validations are passed" in {
      val carAdvert = CarAdvert(2L, "AAA", 1L, 100, true, None, None)
      Post(basePath, toJson(carAdvert)) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
      }
    }

    "list all the car adverts in the database (sorted by id field)" in {
      Get(basePath) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[Seq[CarAdvert]](responseAs[String])
        result.size shouldBe 2
        result.head.title shouldBe "Ad1"
        result.last.title shouldBe "AAA"
      }
    }

    "get the car adverts details by providing the id" in {
      Get(basePath + "/1") ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[Option[CarAdvert]](responseAs[String])
        result should not be empty
        result.get.title shouldBe "Ad1"
      }
    }

    "sort and list all the car adverts based on the sorting field (title)" in {
      Get(basePath + "?sortColumn=title") ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[Seq[CarAdvert]](responseAs[String])
        result.size shouldBe 2
        result.head.title shouldBe "AAA"
        result.last.title shouldBe "Ad1"
      }
    }

    "modify the car advert successfully" in {
      val updateObj = toJson(CarAdvert(2L, "AAA-Updated", 1L, 100, true, None, None))

      Put(basePath + "/2", updateObj) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[CountContainer](responseAs[String])
        result shouldBe CountContainer(1)
      }
    }

    "verify the updated ad" in {
      Get(basePath + "/2") ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[Option[CarAdvert]](responseAs[String])
        result should not be empty
        result.get.title shouldBe "AAA-Updated"
      }
    }

    "delete the car advert successfully" in {
      Delete(basePath + "/2") ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[CountContainer](responseAs[String])
        result shouldBe CountContainer(1)
      }
    }


    "return the deleted count as 0 if id to delete does not exist in database" in {
      Delete(basePath + "/2") ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[CountContainer](responseAs[String])
        result shouldBe CountContainer(0)
      }
    }

    "return the updated count as 0 if the id to modify doesnt exist" in {
      val updateObj = toJson(CarAdvert(2L, "AAA-Updated", 1L, 100, true, None, None))

      Put(basePath + "/2", updateObj) ~> carAdvertRest.route ~> check{
        response.status.intValue() shouldBe 200
        val result = fromJson[CountContainer](responseAs[String])
        result shouldBe CountContainer(0)
      }
    }

  }


}
