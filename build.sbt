name := "auto-listing"

version := "1.0"

scalaVersion := "2.12.10"

val akkaVersion = "2.5.26"

val akka_http_testkit_version    = "10.1.11"

val testDeps = Seq(
  "org.scalatest" %% "scalatest" % "3.0.1" % Test,
  "org.mockito" % "mockito-all" % "2.0.2-beta" % Test,
  "com.h2database" % "h2" % "1.4.200" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % akka_http_testkit_version % Test,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test
)


libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.3.2",
  "org.postgresql" % "postgresql" % "9.4.1211",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2",
  "com.typesafe.akka" %% "akka-http" % akka_http_testkit_version,
  "org.json4s" %% "json4s-jackson" % "3.6.7",
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion
) ++ testDeps

parallelExecution in Test := false